import http from "./http"

type ReturnData = {
    celsius: number
    fahrenheit: number
}
async function convert(celsius: number): Promise<number> {
    const res = await http.get(`/temperature/convert/${celsius}`)
    const convertResult = res.data as ReturnData
    return convertResult.fahrenheit
}
export default { convert }