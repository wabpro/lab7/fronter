import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import temperatureServices from '@/Services/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()

  async function callconvert() {
    loadingStore.doLoad()
    try {
      result.value = await temperatureServices.convert(celsius.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.finish()
  }
  return { valid, celsius, result, callconvert }
})
